JSON-CPP - A Simple C++-11 based parser/management lib for JSON
===============================================================

Building
--------

Build with CMake; pretty much any version of CMake, and any C++-11
capable compiler, should do the trick.


Testing
-------

To help with debugging the parser I added a whole lot of json test
cases to the project already (if anybody has more, feel free to add!).
Test cases are split into two directories: testing/validSyntax for all
those json files that the parser _should_ accept (they are valid
syntax), and testing/invalidSyntax for those that the parser is
supposed to reject.

The cmakefile currently adds all valid cases to automated cmake
testing; to run those, simply do

	 'make && ctest'

in the build directory.

