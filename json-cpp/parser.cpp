// ======================================================================== //
// Copyright 2017 Lukas Leopold Alexander                                   //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "json.h"
#include <sstream>
#include <cassert>

namespace json {

  /*! parse a 'object' type - and throw an exception if this isn't possible ... */
  std::shared_ptr<json::Object> parseObject(const char *&in);
  
  /*! parse what we know - from the grammar - to be a value (either
      because it comes after a "<key>:" or because it is inside an
      array) */
  std::shared_ptr<Value> parseValue(const char *&in);

  /*! consume given character at input loc in; and throw an exception
    if that is not possile.  this checks if the given input locatoin
    matches the 'expectedChar', and if so, consumes it by advancing
    the 'in' pointer; if the input loc does _not_ match this char this
    throws an exceptoi to indicate that the required character could
    not be found */
  void consume(const char *&in, const char expectedChar)
  {
    if (*in == expectedChar)
      ++in;
    else
      throw std::runtime_error(std::string("expected '")+expectedChar
                               +"', but found '"+*in
                               +"' (rest of string: '" +in+"')");
  }


  /*! consume a c++ single-line '//' comment */
  void consumeLineComment(const char *&in)
  {
    consume(in,'/');
    consume(in,'/');
    while (*in && *in != '\n')
      ++in;
  }

  /*! consume a old-school c-style slash-star...start-slash  'block' comment */
  void consumeBlockComment(const char *&in)
  {
    consume(in,'/');
    consume(in,'*');
    while (1) {
      if (*in == 0)
        throw std::runtime_error("json: non-terminated c-style comment");
      if (in[0] == '*' && in[1] == '/') {
        consume(in,'*');
        consume(in,'/');
        return;
      }
      ++in;
    }
  }
  
  /*! consume what we know to be a comment - could still be c-style or
    cpp-style */
  void consumeComment(const char *&in)
  {
    assert(*in == '/');
    if (in[1] == '/')
      consumeLineComment(in);
    else
      consumeBlockComment(in);
  }
  
  /*! skip whitespace ... _AND_ comments - we simply treat comments as
      whitespace */
  void skipWhites(const char *&in)
  {
    while (1) {
      /* first, skip 'real' whitespaces */
      while (*in == ' ' || *in == '\t' || *in == '\n' || *in == '\r')
        ++in;
      
      /* now, test if this is a comment ... */
      if (*in == '/') {
        // .... and if so, consume it
        consumeComment(in);
        // .. then continue with whitespaces
        continue;
      }
      else
        // not a comment, so done for now
        break;
    }
  }

  /* parse the 'digits' of a numerical value - ie, all the stuff
     _after_ a optoinal sign, and _before_ any '.' or 'e' */
  std::string parseDigits(const char *&in)
  {
    std::stringstream ss;
    while (isdigit(*in)) ss << *in++;
    return ss.str();
  } 

  /* parse the mantissa of a float number (ie, everything starting
     with (and including) the '.' to the 'e' (or end of number), if
     applicable; return empty string if no mantissa found */
  std::string parseMantissa(const char *&in)
  {
    if (*in != '.') return "";

    std::stringstream ss;
    ss << *in++; // '.'
    while (isdigit(*in)) ss << *in++;
    return ss.str();
  }

  /* parse the exponent of a float number (ie, all the 'e[+/-]<digits>
     of a float).  if not found, return empty string */
  std::string parseExponent(const char *&in)
  {
    if (*in != 'e') return "";
    std::stringstream ss;
    ss << *in++; // 'e'

    if (*in == '-' || *in == '+')
      ss << *in++;

    std::string digits = parseDigits(in);
    if (digits == "")
      throw std::runtime_error("mal-formed floating point value");

    return ss.str()+digits;
  }
  
  std::shared_ptr<Value> parseNumericalValue(const char *&in)
  {
    // save the old pointer, for debugging output
    const char *originalInput = in;
    
    // parse the sign, if set
    std::string sign = (*in == '-')?"-":"";
    
    // eat the sign, if set
    if (*in == '-' || *in == '+') ++in;
    
    /* digits - all the stuff _before_ any '.' or 'e' */
    std::string digits = parseDigits(in);
    
    /* mantissa - everything starting with '.' to the 'e' (or end of
       number), if applicable */
    std::string mantissa = parseMantissa(in);
      
    /* exponent - all the 'e[+/-]<digits> */
    std::string exponent = parseExponent(in);
      
    if (mantissa == "" && exponent == "") {
      if (digits == "")
        throw std::runtime_error("not a valid number: sign='"+sign+"', digits='"+digits+"', mantissa = '"+mantissa+"', exponent='"+exponent+"'"+"\n(at input '"+originalInput+"')");
      std::string number = sign + digits;
      try {
        if (sign != "-")
          return std::make_shared<UIntValue>(std::stoull(number));
        else
          return std::make_shared<IntValue>(std::stoll(number));
      } catch (std::out_of_range) {
        std::cout << "json: warning - this json file contains a 'integer' number that cannot be represented in 64 bit signed or unsigned integers. Converting this to a double, but this _will_ lose precision (double only has 48 bits of mantissa" << std::endl;
        return std::make_shared<FloatValue>(std::stod(number));
      }
    } else {
      std::string number = sign + digits + mantissa + exponent;
      return std::make_shared<FloatValue>(std::stod(number));
    }
    throw std::runtime_error("seems to be a float, but this is not implemneted yet");
  }
  
  /*! parse what we know to be a string literal. ie, we know it starts
      with '"', and we'll parse til the end of this string 

      currently we're not doing escapes, yet - is this even allowed in json?
  */
  std::string parseStringLiteral(const char *&in)
  {
    consume(in,'"');
    std::stringstream ss;
    while (*in && *in != '"')
      ss << *in++;
    consume(in,'"');    
    return ss.str();
  }

  /*! parse what we know to be a bool value. this can be 'false' or
      'true'; anything else is an error */
  std::shared_ptr<Value> parseBoolValue(const char *&in)
  {
    bool b = (*in == 't');
    if (b) {
      consume(in,'t');
      consume(in,'r');
      consume(in,'u');
      consume(in,'e');
    } else {
      consume(in,'f');
      consume(in,'a');
      consume(in,'l');
      consume(in,'s');
      consume(in,'e');
    }
    return std::make_shared<BoolValue>(b);
  }

  /*! parse what we know to be a bool value. this can be 'false' or
      'true'; anything else is an error */
  std::shared_ptr<Value> parseNullValue(const char *&in)
  {
    consume(in,'n');
    consume(in,'u');
    consume(in,'l');
    consume(in,'l');
    return std::make_shared<NullValue>();
  }

  /*! parse what we know to be an array - ie, we know it starts with
      '[' and goes all the way to the closing ']' */
  std::shared_ptr<ArrayValue> parseArrayValue(const char *&in)
  {
    std::shared_ptr<ArrayValue> array = std::make_shared<ArrayValue>();
    consume(in,'[');
    skipWhites(in);
    if (*in != ']') {
      array->add(parseValue(in));
      skipWhites(in);
      while (*in == ',') {
        consume(in,',');
        skipWhites(in);
        array->add(parseValue(in));
        skipWhites(in);
      }
    }
    consume(in,']');
    return array;
  }

  /*! what what we know to be "a" value - might be a object, string,
      array, or numerical value */
  std::shared_ptr<Value> parseValue(const char *&in)
  {
    skipWhites(in);
    if (*in == '{') 
      return std::make_shared<ObjectValue>(parseObject(in));

    if (*in == '"') 
      return std::make_shared<StringValue>(parseStringLiteral(in));

    if (*in == '[')
      return parseArrayValue(in);

    if (*in == 't' || *in == 'f')
      return parseBoolValue(in);

    if (*in == 'n')
      return parseNullValue(in);
    
    return parseNumericalValue(in);
  }
  
  std::shared_ptr<Pair> parsePair(const char *&in)
  {
    skipWhites(in);
    std::string key = parseStringLiteral(in);

    skipWhites(in);
    consume(in,':');
    
    skipWhites(in);
    
    return std::make_shared<Pair>(key,parseValue(in));
  }
  
  /*! parse the pairs in a what we know to be non-empty object */
  void parseObjectPairs(std::shared_ptr<Object> obj, const char *&in)
  {
    /* there can't be any whitespace here - we've already tested that
       it's not a '}' so the whitespace was already eaten */
    obj->add(parsePair(in));
    skipWhites(in);
    while (*in == ',') {
      consume(in,',');
      obj->add(parsePair(in));
      skipWhites(in);
    }
  }
  
  /*! parse what we know to be a'object' type - and throw an exception
      if this isn't possible ... */
  std::shared_ptr<json::Object> parseObject(const char *&in)
  {
    std::shared_ptr<json::Object> obj = makeObject();

    skipWhites(in);
    consume(in,'{');

    skipWhites(in);

    if (*in != '}') 
      parseObjectPairs(obj,in);
    
    skipWhites(in);
    consume(in,'}');
    
    return obj;
  }
  
  /*! parse a json string */
  std::shared_ptr<json::Value> parseFromString(const std::string &input)
  {
    const char *in = input.c_str();
    skipWhites(in);
    return parseValue(in);
    // return parseObject(in);
  }
}
