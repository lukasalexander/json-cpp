// ======================================================================== //
// Copyright 2017 Lukas Leopold Alexander                                   //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "json.h"
#include <sstream>

namespace json {
  
  // ==================================================================
  /* helper functoins to create objects, key-value pairs, etc */
  // ==================================================================

  /*! make a new key/int-value pair */
  std::shared_ptr<json::Pair> makePair(const std::string &name,
                                       const int64_t &value)
  {
    return std::make_shared<json::Pair>(name,std::make_shared<json::IntValue>(value));
  }

  /*! make a new key/object-value pair */
  std::shared_ptr<json::Pair> makePair(const std::string &name,
                                       const std::shared_ptr<Object> &obj)
  {
    return std::make_shared<json::Pair>(name,std::make_shared<json::ObjectValue>(obj));
  }

  /*! make a new object. you can also do makeObject->add(...)->add(...) */
  std::shared_ptr<json::Object> makeObject() 
  {
    return std::make_shared<json::Object>();
  }

  std::string Pair::toString() const
  {
    std::stringstream ss;
    ss << "\"" << key << "\"" << ":" << value->toString();
    return ss.str();
  }


  /*! add one more pair to this object's list of key-value pairs */
  std::shared_ptr<Object> Object::add(const std::shared_ptr<Pair> &addtlPair)
  {
    pairs.push_back(addtlPair);
    return shared_from_this();
  }

  
  std::string Object::toString() const
  {
    std::stringstream ss;
    ss << "{";
    for (auto it = pairs.begin(); it != pairs.end(); it++) {
      if (it != pairs.begin())
        ss << ",";
      ss << (*it)->toString();
    }
    ss << "}";
    return ss.str();
  }

  /*! convert to a json-string */
  std::string ArrayValue::toString() const
  {
    std::stringstream ss;
    ss << "[";
    for (auto it = elements.begin(); it != elements.end(); it++) {
      if (it != elements.begin())
        ss << ",";
      ss << (*it)->toString();
    }
    ss << "]";
    return ss.str();
  }

  /*! add one more value to this array, and reutrn reference to
    ourselves, so we can tho makeArrayValue->add(..)->add(...)
    etc */
  std::shared_ptr<ArrayValue> ArrayValue::add(const std::shared_ptr<Value> &addtlValue)
  {
    elements.push_back(addtlValue);
    return std::dynamic_pointer_cast<ArrayValue>(shared_from_this());
  }



  /*! check if the given value - whatever flavor of value it might be
    - has a child value 'key', and return true if so; false
    otherwise */
  bool Value::has(const std::string &key) const
  {
    /* typically values don't have children, period - objectvalue will
       override this */
    return false;
  }

  /*! check if the given value - whatever flavor of value it might be
    - has a child value 'key', and return true if so; false
    otherwise */
  bool ObjectValue::has(const std::string &key) const
  {
    return value->has(key);
  }

    /*! check if this object has a child value of name 'key' */
  bool Object::has(const std::string &key) const
  {
    for (auto it : pairs)
      if (it->getKey() == key)
        return true;
    return false;
  }

  Value &Object::operator[](const std::string &key) const
  {
    for (auto it : pairs)
      if (it->getKey() == key)
        return *it->getValue();
    
    throw std::runtime_error("json::Object doesn't have a member of this key");
  }

  
}

