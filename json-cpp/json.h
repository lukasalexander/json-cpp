// ======================================================================== //
// Copyright 2017 Lukas Leopold Alexander                                   //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include <memory>
#include <iostream>
#include <string>
#include <vector>

#include <stdint.h>

namespace json {

  /*! abstract base class for a value */
  struct Value : public std::enable_shared_from_this<Value> {

    /*! check if the given value - whatever flavor of value it might
      be - has a child value 'key', and return true if so; false otherwise */
    virtual bool has(const std::string &key) const;

    virtual int64_t asInt() const { throw std::runtime_error("Value::asInt() on non-int json value"); }
    virtual std::string asString() const { throw std::runtime_error("Value::asString() on non-string json value"); }
    
    /*! returns a reference to the child value named 'key', if such
        exists; or throws exception if it doesn't */
    virtual Value &operator[](const std::string &key) const
    { throw std::runtime_error("invalid array access on value that does not contain children"); }
    
    /*! convert to a json-string */
    virtual std::string toString() const = 0;
  };

  /*! a key-value pair */
  struct Pair {
    Pair(const std::string key, const std::shared_ptr<Value> &value)
      : key(key), value(value)
    {}

    /*! convert to a json-string */
    virtual std::string toString() const;

    std::string getKey() const { return key; }
    std::shared_ptr<Value> getValue() const { return value; }
  private:
    const std::string            key;
    const std::shared_ptr<Value> value;
  };

  /*! a base json object */
  struct Object : public std::enable_shared_from_this<Object> { 

    /*! convert to a json-string */
    std::string toString() const;

    /*! add one more pair to this object's list of key-value pairs */
    std::shared_ptr<Object> add(const std::shared_ptr<Pair> &addtlPair);
    
    /*! add a new key/int-value pair */
    template<typename T>
    std::shared_ptr<Object> add(const std::string &name, const T &value);

    /*! check if this object has a child value of name 'key' */
    bool has(const std::string &key) const;

    virtual Value &operator[](const std::string &key) const;
    
  private:
    std::vector<std::shared_ptr<Pair>> pairs;
  };


  /*! a value containing a (64-bit) integer type */
  struct IntValue : public Value {
    IntValue(int64_t value) : value(value) {}

    /*! convert to a json-string */
    virtual std::string toString() const { return std::to_string(value); }

    virtual int64_t asInt() const override { return value; }
    virtual std::string asString() const override { return std::to_string(value); }

  private:
    const int64_t value;
  };

  /*! a value containing a (64-bit) integer type */
  struct UIntValue : public Value {
    UIntValue(uint64_t value) : value(value) {}

    /*! convert to a json-string */
    virtual std::string toString() const { return std::to_string(value); }

  private:
    const uint64_t value;
  };

  /*! a value containing a boolean (true/false) type */
  struct BoolValue : public Value {
    BoolValue(bool value) : value(value) {}

    /*! convert to a json-string */
    virtual std::string toString() const { return value?"true":"false"; }

  private:
    const bool value;
  };

  /*! a value containing a null reference */
  struct NullValue : public Value {
    NullValue() {}

    /*! convert to a json-string */
    virtual std::string toString() const { return "null"; }
  };
  
  /*! a value containing a (double precision) floating point value */
  struct FloatValue : public Value {
    FloatValue(double value) : value(value) {}

    /*! convert to a json-string */
    virtual std::string toString() const { return std::to_string(value); }

  private:
    const double value;
  };

  /*! a value containing a string literal */
  struct StringValue : public Value {
    StringValue(const std::string &value) : value(value) {}

    /*! convert to a json-string */
    virtual std::string toString() const { return '"'+value+'"'; }

    virtual int64_t asInt() const override { return stoi(value); }
    virtual std::string asString() const override { return value; }
    
  private:
    const std::string value;
  };

  /*! a value containing an object referejce (ie, a child node) */
  struct ObjectValue : public Value {
    ObjectValue(const std::shared_ptr<Object> &value) : value(value) {}

    /*! check if the given value - whatever flavor of value it might
      be - has a child value 'key', and return true if so; false otherwise */
    virtual bool has(const std::string &key) const override;

    /*! convert to a json-string */
    virtual std::string toString() const { return value->toString(); }

    virtual Value &operator[](const std::string &key) const
    { return (*value)[key]; }

  private:
    std::shared_ptr<Object> value;
  };

  /*! a value that is itself an array of values */
  struct ArrayValue : public Value {
    ArrayValue() {}

    /*! convert to a json-string */
    virtual std::string toString() const override;

    /*! add one more value to this array, and reutrn reference to
        ourselves, so we can tho makeArrayValue->add(..)->add(...)
        etc */
    std::shared_ptr<ArrayValue> add(const std::shared_ptr<Value> &addtlValue);

  private:
    std::vector<std::shared_ptr<Value>> elements;
  };


  // ==================================================================
  /* helper functions to create objects, key-value pairs, etc */
  // ==================================================================

  /*! parse a json string */
  std::shared_ptr<json::Value> parseFromString(const std::string &input);
  
  /*! make a new object. you can also do makeObject->add(...)->add(...) */
  std::shared_ptr<json::Object> makeObject();
  
  /*! make a new object. you can also do
      makeObject->add(...)->add(...). (same as 'makeObject', just
      sometimes more readable) */
  inline std::shared_ptr<json::Object> emptyObject() { return makeObject(); }
    
  /*! make a new key/int-value pair */
  std::shared_ptr<json::Pair> makePair(const std::string &name,
                                       const int64_t &value);

  /*! make a new key/object-value pair */
  std::shared_ptr<json::Pair> makePair(const std::string &name,
                                       const std::shared_ptr<Object> &value);



  /*! add a new key/int-value pair */
  template<typename T>
  inline std::shared_ptr<Object> Object::add(const std::string &name, const T &value)
  { return add(makePair(name,value)); }
    
  
}
