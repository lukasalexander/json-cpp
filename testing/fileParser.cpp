// ======================================================================== //
// Copyright 2017 Lukas Leopold Alexander                                   //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

/*! \file parseFile.cpp Simple test app that parses a .json file and
  prints its output - just to see that the parser can parse that file
  w/o throwing any exceptions etc */

#include "json-cpp/json.h"
#include <fstream>
#include <sstream>

/* read the file's content into a string */
std::string readFile(const std::string &fileName)
{
  std::ifstream ifs(fileName, std::ifstream::in);

  std::stringstream ss;
  char c = ifs.get();
  while (ifs.good()) {
    ss << c;
    c = ifs.get();
  }
  return ss.str();
}

/*! read the file, parse it into a json object, and (re-)print that
    jason object */
void parseFile(const std::string &fileName)
{
  /* read the file's content into a string */
  std::string testString = readFile(fileName);
  
  /* read the file's content into a string */
  std::cout << "done reading file " << fileName << ":" << std::endl;
  std::shared_ptr<json::Value> parsed = json::parseFromString(testString);
  
  std::cout << "- parser input: " << testString << std::endl;
  std::cout << "- parsed ouput: " << parsed->toString() << std::endl;
}

int main(int ac, const char **av)
{
  if (ac == 1)
    throw std::runtime_error("no input files.\nUsage: ./parseFileTest <inputFile.json>(*)");

  for (int i=1;i<ac;i++)
    parseFile(av[i]);
}
